from time import time

import pytest
from peewee import IntegrityError

from tokenstore import TokenStore
from tokenstore.models import Token


@pytest.fixture(scope="module", autouse=True)
def store():
    with TokenStore(":memory:") as s:
        s.init()
        yield s


def test_new_token(store):
    for i, n in enumerate([32, 64, 128]):
        key, token = store.new_token(f"client{i}", nbytes=n)
        assert isinstance(token, Token)
        with pytest.raises(IntegrityError):
            store.new_token(f"client{i}")


def test_get_token(store):
    key, token = store.new_token(str(time()))

    assert store.get_token(key) == token
    assert store.get_token(f".{key}") is None
    assert store.get_token(f"{key}.") is None

    assert store.get_token(id_=token.id) == token
    assert store.get_token(id_=-1) != token

    client = key.split(".")[0]
    assert store.get_token(client=client) == token
    assert store.get_token(client="") != token


def test_enable_token(store):
    _, token = store.new_token(str(time()))
    assert token.enabled

    token.enabled = False
    assert token.save() == 1

    token = store.get_token(id_=token.id)
    assert not token.enabled


def test_delete_token(store):
    _, token = store.new_token(str(time()))
    assert token.delete_instance() == 1
    assert store.get_token(id_=token.id) is None
