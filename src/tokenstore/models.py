from datetime import datetime

from peewee import (
    BooleanField,
    CharField,
    CompositeKey,
    DateTimeField,
    ForeignKeyField,
    IntegerField,
    Model,
)


class Token(Model):
    id = IntegerField(primary_key=True)
    client = CharField(64, unique=True)
    digest = CharField(128, unique=True)
    enabled = BooleanField(default=True)
    date_created = DateTimeField(default=datetime.now)


class Role(Model):
    id = IntegerField(primary_key=True)
    name = CharField(32, unique=True)


class TokenRole(Model):
    token = ForeignKeyField(
        Token, backref="roles", on_delete="CASCADE", on_update="CASCADE"
    )
    role = ForeignKeyField(
        Role, backref="tokens", on_delete="CASCADE", on_update="CASCADE"
    )

    class Meta:
        primary_key = CompositeKey("token", "role")
