import secrets
from typing import Tuple, Union

from passlib.hash import bcrypt
from passlib.ifc import PasswordHash
from peewee import Database, SqliteDatabase

from .models import Role, Token, TokenRole

__version__ = "0.0.0a0"


class TokenStore:
    _models = [Role, Token, TokenRole]

    def __init__(self, db: Union[str, Database], hasher: PasswordHash = None):
        if isinstance(db, Database):
            self.db = db
        elif isinstance(db, str):
            self.db = SqliteDatabase(db, pragmas={"foreign_keys": 1})
        else:
            raise TypeError("db must be a str or Database")
        self.db.bind(self._models)

        if hasher is None:
            self.hasher = bcrypt
        elif isinstance(hasher, PasswordHash):
            self.hasher = hasher
        else:
            raise TypeError("hasher must be None or a PasswordHash")

    def __enter__(self):
        self.db.connect()
        return self

    def __exit__(self, type_, value, traceback):
        self.db.close()

    def init(self):
        self.db.create_tables(self._models)

    def new_token(self, client: str, nbytes: int = 32) -> Tuple[str, Token]:
        """Create a new token"""
        client = "".join(client.split()).replace(".", "")
        secret = secrets.token_urlsafe(nbytes)
        digest = self.hasher.hash(secret)
        t = Token.create(client=client, digest=digest)
        return f"{client}.{secret}", t

    def get_token(
        self, key: str = None, *, client: str = None, id_: int = None
    ) -> Union[Token, None]:
        """Fetch a token"""
        if key is not None:
            client, *rest = key.split(".")
            secret = ".".join(rest)
            t = Token.get_or_none(Token.client == client)
            return t if t and self.hasher.verify(secret, t.digest) else None
        elif client is not None:
            return Token.get_or_none(Token.client == client)
        elif id_ is not None:
            return Token.get_or_none(Token.id == id_)
        raise TypeError("Missing at least one argument")

    def list_tokens(self, client: str = None, enabled: bool = None) -> None:
        """List tokens by client or enabled"""
        q = Token.select()
        if client:
            q = q.where(Token.client == client)
        if enabled is not None:
            q = q.where(Token.enabled == enabled)
        for t in q.execute():
            print(repr(t))
